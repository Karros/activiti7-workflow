/**
 * @description 用户相关的接口请求
 * @author zr
 */
import request from '@/utils/request'
import qs from 'qs'

export const login = (data) => {
  return request({
    url: 'login',
    method: 'POST',
    data: qs.stringify(data)
  })
}

export const getUserInfo = (data) => {
  return request({
    url: '/admin/user/getUserInfo.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

export const logout = () => {
  return request({
    url: '/admin/user/logout.do',
    method: 'post'
  })
}

// 查询用户列表
export const getUserList = (data) => {
  return request({
    url: '/admin/user/getUserList.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 获取所有用户类型
 * @returns 返回结果
 */
export const getAllUserType = () => {
  return request({
    url: '/admin/user/getAllUserType.do',
    method: 'POST'
  })
}

/**
 * 检查用户名是否已经存在
 *
 * @export
 * @param {string} userId 用户id
 * @return {*} 返回结果
 */
export const checkUserIdIsExist = (data) => {
  return request({
    url: '/admin/user/checkUserIdIsExist.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 新增用户
 *
 * @export
 * @param {Object} data 新增参数{ userId,userName, userTypeId,roleIdList, mobile,email,districtId, departmentId,active}
 * @return {*}
 */
export const addUser = (data) => {
  return request({
    url: '/admin/user/addUser.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 修该用户信息
 *
 * @export
 * @param {Object} data 新增参数{ userId,userName, userTypeId,roleIdList, mobile,email,districtId, departmentId,active}
 * @return {*}
 */
export const updateUser = (data) => {
  return request({
    url: '/admin/user/updateUser.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 删除用户
 *
 * @export
 * @param {String} userId 用户ID
 * @return {*}
 */
export const deleteUser = (data) => {
  return request({
    url: '/admin/user/deleteUser.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 获取角色列表
 *
 * @export
 * @param {String} pageNum 页码
 * @param {String} pageSize 每页条数
 * @return {*}
 */
export const getRoleList = (data) => {
  return request({
    url: '/admin/role/getRoleList.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 添加角色信息
 *
 * @export
 * @param {String} rights 角色权限
 * @param {String} roleDesc 角色描述
 * @param {String} roleName 角色名称
 * @return {*}
 */
export const addRole = (data) => {
  return request({
    url: '/admin/role/addRole.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 获取角色信息
 *
 * @export
 * @param {String} roleId 角色id
 * @return {*}
 */
export const getRoleInfoByRoleId = (data) => {
  return request({
    url: '/admin/role/getRoleInfoByRoleId.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 修改角色信息
 *
 * @export
 * @param {String} roleId 角色id
 * @return {*}
 */
export const updateRole = (data) => {
  return request({
    url: '/admin/role/updateRole.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 删除角色信息
 *
 * @export
 * @param {String} roleId 角色id
 * @return {*}
 */
export const deleteRole = (data) => {
  return request({
    url: '/admin/role/deleteRole.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 重置密码
 *
 * @export
 * @param {String} userId 用户ID
 * @return {*}
 */
export const resetPassword = (data) => {
  return request({
    url: '/admin/user/resetPassword.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 修改密码
 *
 * @export
 * @param {String} passwordNew 新密码
 * @param {String} passwordOld 原密码
 * @return {*}
 */
export const updatePassword = (data) => {
  return request({
    url: '/admin/user/updatePassword.do',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 日志管理- 列表
 *
 * @export
 * @param {String} endTime    数据结束时间
 * @param {String} operation  操作
 * @param {Number} pageNum    页码
 * @param {Number} pageSize   分页大小
 * @param {String} platForm   平台
 * @param {String} starTime   数据开始时间
 * @param {String} userName   用户名
 * @return {*}
 */
export const getOperationLogList = (data) => {
  return request({
    url: '/sys/operationLog/getOperationLogList.do',
    method: 'GET',
    params: data
  })
}

/**
 * 日志管理- 列表
 *
 * @export
 * @param {String} endTime    数据结束时间
 * @param {String} operation  操作
 * @param {Number} pageNum    页码
 * @param {Number} pageSize   分页大小
 * @param {String} platForm   平台
 * @param {String} starTime   数据开始时间
 * @param {String} userName   用户名
 * @return {*}
 */
export const getLogList = (data) => {
  return request({
    url: '/log/getLogList.do',
    method: 'POST',
    params: data
  })
}

/**
 *  根据名字查找用户，用于批量更新用户角色
 * @param {String} userName 用户姓名
 * @returns
 */
export const getAllUserByName = (data) => {
  return request({
    url: '/admin/role/getAllUserByName.do',
    method: 'POST',
    params: data
  })
}

/**
 * 批量更新用户角色
 * @param {String} roleId	  角色id
 * @param {Array} users 用户集合
 * @returns
 */

export const updateUserRole = (data) => {
  return request({
    url: '/admin/role/updateUserRole.do',
    method: 'POST',
    params: data
  })
}
